# FAURIE Lilian
# Master 1 - Bioinformatique
# Projet Python 2022


#-----------------------Bibliothèques-----------------------#

import re
from math import sqrt
from collections import defaultdict
import matplotlib.pyplot as plt

#-------------------------Fonctions-------------------------#

# Définition fonction moyenne

def moyenne(liste) :
	sum = 0
	for nb in liste : 
		sum+=nb
	return(sum/len(liste))

# Définition fonction écart-type

def ecart_type(liste):
	moy = moyenne(liste)
	sumdistance = 0
	for nb in liste : 
		distance = (nb-moy)**2
		sumdistance+=distance
	return(sqrt(sumdistance/len(liste)))

#--------------------Variables exercice n°1--------------------#

listeseq=[]
listeseq_outN=[]
listeAT=[] 
listeresult =[]
nbseq = 0
dictATCG = defaultdict(int)	

#-------------------------Exercice n°1-------------------------#

# Importation du fichier "fragments1000.faa" :

with open( "fragments1000.faa" , "r") as f1 :
	for li in f1 : 
		li = li.rstrip()
	
# Question n°1 : Calculer le nombre de séquences de départ.

# Compte toutes les lignes commençant par ">" est les stocks dans la variable "nbseq". Ces lignes correspondent aux "titres" des séquences nucléotidiques.

		if li.startswith(">") :
			nbseq+=1

# Question n°2 : Calculer la moyenne et l'écart-type pour la longueur des fragments séquencés.

# Si les lignes ne comportent pas le caractère ">", on ajoute leurs nombres à la liste "listeseq". On applique ensuite a cette liste, les fonctions moyenne et écart-type (définis en amount) afin d'obtenir la moyenne et l'écartype de la liste. A noter que les lignes ne comportant pas le caractère ">" correspondent aux séquences nucléotidiques. 

		if ">" not in li :
			listeseq.append(len(li))
			moyenne_Q2 = moyenne(listeseq)
			ecart_typeQ2 = ecart_type(listeseq)

# Question n°3 : Déterminer si les lettres A, T, C et G sont les seules lettres présentes dans les séquences ?
 
# Si les lignes ne comportent pas le caractère ">", on les passent toutes en majuscules et on les stocks dans la variable "liUP". On applique la fonction re.findall a cette variable afin de rechercher tous les "N" présent dans ces lignes. On stock le nombre de "N" trouvé dans la variable "Ncount". On compte le nombre de "N" trouvé dans les lignes et on ajoute cette valeur à la clé [N] du dictionnaire "dictATCG" au préalablement créé.
 
			liUP = li.upper()
			Ncount = re.findall('[N]', liUP)
			for N in Ncount:
				dictATCG[N]+=1

# Question n°4	: Calculer la moyenne et l'écart-type du pourcentage de AT des fragments séquencés

# Afin de calculer la moyenne et l'écart-type du pourcentage de AT des fragments séquencés, on ajoute dans une liste nommée "listeAT" le nombre de nucléotide "A" et "T" présent dans les lignes.

			listeAT.append(len(re.findall('[AT]', liUP)))
			listeseq_outN.append(len(li)- len(Ncount))

# Afin d'obtenir le pourcentage de AT des fragments séquencés, on divise les nombres de valeur d'AT par ligne dans la liste "listeAT" par le nombre total de nucléotides (sans N) par ligne dans la liste "listeseq_outN". On multiplie par cent pour obtenir des poucentages et on les stocks dans la liste "listeresult". 

for i in range(len(listeAT)) :
	listeresult.append(listeAT[i] / listeseq_outN[i] * 100)

# On applique ensuite à cette liste les fonctions moyenne et écart-type afin d'obtenir la moyenne et l'écart-type de la liste. 

moyenne_Q4 = moyenne(listeresult)
ecart_typeQ4 = ecart_type(listeresult)

#---------------------Réponse exercice n°1---------------------#	
				
print("\n I. Vérification de la qualité de nos séquences de départ\n")

print("Question n°1 : Le fichier comporte",nbseq,"séquences de départ.\n")

print("Question n°2 : La moyenne de la longueur des fragments séquencés est de :", moyenne_Q2,". La valeur de l'écart type est de :", round(ecart_typeQ2,3),"\n")

for i in dictATCG:				
	print ("Question n°3 : Les séquences nucléotidiques ne contiennent pas uniquement des nucléotides A,T,C et G. Il y a en effet la présence de", dictATCG[N],"nucléotides",N,"dans les séquences.\n")

print("Question n°4 : La moyenne du pourcentage de AT des fragments séquencés est de :",round(moyenne_Q4,3),". L'écart-type de ce pourcentage est de :", round(ecart_typeQ4,3))

#--------------------Variables exercice n°2--------------------#

nbseqblast = 0
seqcount = 0
listeSCORE=[]
listeSEQ=[]
score=[]
liste_tax_blast=[]
liste_tax_blast_signf=[]
dictFRAG = defaultdict(bool)
dictCLONE = defaultdict(bool)
dictCLONE2 = defaultdict(list)
dictTAXFRAG = defaultdict(list)

#-------------------------Exercice n°2-------------------------#

# Importation du fichier "resultat_Blast1000.tab" :

with open( "resultat_Blast1000.tab" , "r") as f2 :
	for li in f2 :	
	
# Question n°1 : Calculer le nombre de résultats de BLAST.

# Si la ligne commence par la chaîne de caractère "clone", on compte le nombre de ligne comportant cette chaine de caractère et on stock le résultat dans la variables "nbseqblast". 

		if li.startswith("clone") :
			nbseqblast+=1
			li = li.split("\t")
			id_tax_frag = li[1].split(":")[0] #Partie III - Q3
			dictTAXFRAG[li[0]].append(id_tax_frag) #Partie III - Q4
			clone_id = li[0].split("_")[0] #Partie III - Q5
			
# Question n°2 : Calculer le nombre de résultats de BLAST significatifs.

# On converti les colonnes 10 (e-value) et 11 (score) en valeur numérique. On applique ensuite les conditions de Blast siginificatif (e-value < 10e-5 et un score > 50) à une variable compteur "seqcount" pour obtenir le résultat correspondant. 

			li[10]=float(li[10])
			li[11]=float(li[11])
			
			if li[10] < 10**-5 and li[11] > 50: 
				
				seqcount+=1
				liste_tax_blast.append(id_tax_frag) #Partie III - Q3
				id_tax_frag_signf = li[1] #Partie IV - Q4.a
				liste_tax_blast_signf.append(id_tax_frag_signf) #Partie IV - Q4.a
	
# Question n°3 : Calculer la moyenne des scores de BLAST significatifs.

# Afin de calculer la moyenne des scores, on récupére dans une liste nommée "listeSCORE", l'ensemble les valeurs de la onzième colonne. On applique la fonction moyenne à cette liste afin de récupérer la moyenne de la liste. On stock cette valeur dans la variable "moy_listeSCORE".

				listeSCORE.append(li[11])
				moy_listeSCORE = moyenne(listeSCORE)
			
# Question n°4 : Calculer le nombre de fragments ayant au moins une séquence significativement similaire dans KEGG.

# On récupére en tant que clé dans le dictionnaire "dictFRAG", les noms des clones de la colonne 1. Pour chaque fragments on attribut une valeur booléenne "TRUE" afin de le marquer. On stock ensuite dans une variable "fragment", le nombre totale de valeur "TRUE" du dictionnaire. 
			
				dictFRAG[li[0]] = True
				fragments = len(dictFRAG)
				
# Question n°5	: Calculer le nombre de clones ayant au moins une séquence significativement similaire dans KEGG.

# On récupére en tant que clé dans le dictionnaire "dictCLONE", les noms des clones de la colonne 1 en leur supprimant "_deb" et "_fin" de leurs noms. Cela permet de récuperer le nombre de clone et non le nombre de fragments ayant au moins une séquence significativement similaire dans KEGG. Comme pour la question précedente. Pour chaque clone on attribut une valeur booléenne "TRUE" afin de le marquer. On stock ensuite dans une variable "clone", le nombre totale de valeur "TRUE" du dictionnaire. 
				
				dictCLONE[li[0][:-4]] = True
				dictCLONE2[clone_id].append(id_tax_frag) #Partie III - Q5
				clone = (len(dictCLONE))
				
# Question n°6 : Afficher les 5 alignements ayant les meilleurs scores de BLAST

# Pour afficher les 5 alignements ayant les meilleurs scores de BLAST, on créer une liste "score" stockant l'emsemble des informations des fragments. On applique une fonction de tri à cette liste permettant de classer en fonction des valeurs croissante de scores. Il suffit ensuite d'afficher les 5 premières valeurs avec une boucle allant de un à cinq.
		
			score.append(li)
score = sorted(score, key=lambda score: score[11], reverse = True)	

#---------------------Réponse exercice n°2---------------------#

print("\n\n II. Analyse rapide des résultats de Blast\n")

print("Question n°1 : Il y a", nbseqblast, "résultats BLAST.\n")

print("Question n°2 : Il y a", seqcount,"résultats de BLAST significatifs. ( e-value < 10e-5 et un score > 50)\n")

print("Question n°3 : La moyenne des scores de BLAST significatifs est de : ",round(moy_listeSCORE,3),"\n")

print("Question n°4 : Le nombre de fragments ayant au moins une séquence significativement similaire dans KEGG est de : ",fragments,"\n")

print("Question n°5 : Le nombre de clones ayant au moins une séquence significativement similaire dans KEGG est de :",clone, "\n")

print("Question n°6 : Les 5 alignements ayant les meilleurs scores de BLAST sont :\n")
for i in range (5) :
	print (score[i])

#--------------------Variables exercice n°3--------------------#

flagACTINO = 0
sumACTINO=0
dictCLADE = defaultdict(int)
dictTAX = defaultdict(list)
dictSIGN_BLAST = defaultdict(int)
dictBEST_BLAST = defaultdict(int) 
clone_tax = 0
id_actino = []
graph3x = []
graph3y = []

#-------------------------Exercice n°3-------------------------#		
	
# Importation du fichier "taxonomy" :

with open( "taxonomy" , "r") as f3 :
	for li in f3 :
	
# Question n°1 : Combien d'actinobactéries entièrement séquencées retrouve-on dans KEGG ?

# Pour récupérer le nombre d'actinobactéries entièrement séquencées dans KEGG, on utilise ici un système de marquage flag. Lorsque la ligne commence par la chaîne de caractère "### Actinobacteria", on attribu a la ligne la valeur 1. Si la ligne ne commence pas par la chaîne de caractère "### Actinobacteria" et que le premier caractère de la ligne ne commence pas par "T", alors on attribut a la ligne la valeur 0. Ce marquage permet de sélectionner les lignes d'intéret. Au final, il suffit donc de compter les lignes ayant la valeur 1 et n'ayant pas la chaîne de caractère "### Actinobacteria". On stock le résultat du compteur dans la variale "sumACTINO".

		if li.startswith ("### Actinobacteria") :
			flagACTINO = 1
		
		if not li.startswith ("### Actinobacteria") :
			if li[0] != "T" :
				flagACTINO = 0
			
		if flagACTINO == 1 and not "### Actinobacteria" in li :
			sumACTINO+=1	
			id_actino.append(li.split("\t")[1]) #

# Question n°2 : Pour chaque clade ayant plus de 13 génomes séquencés, afficher le nombre de génomes dans KEGG ?

# On cherche à faire un dictionnaire ayant pour clé, les noms des clades et en valeurs le nombre de génome séquencés. Pour cela on segmente les lignes a chaque tabulation. Si la ligne commence par la chaîne de caractère "### ", on la  récupére en supprimant les quatre premiers caractères (### ) et on la stock dans la variable "clade". Cette étape permet de récupérer les noms des clades de niveau 3. Si il n'y a pas le caractère "#" dans la ligne, on définit alors le nom des clades en tant que clé dans le dictionnaire "dictCLADE" et on compte le nombre de ligne par clé.

		li = li.rstrip()
		if li.startswith ("### ") :
			clade=li[4:]
		if "#" not in li:
			dictCLADE[clade]+=1
			
			
# Question n°3 : Calculer le nombre de BLAST significatifs obtenus entre des fragments et chaque clade.

# On va chercher a comparer le dictionnaire "dictTAX" comportant les noms des clades comme clés et les identifiants de taxons comme valeur avec une liste d'identifiants de taxons significatifs "liste_tax_blast" issuent du fichier resultat_Blast1000.tab. 

			liste_tax_taxonomy = li.split("\t")
			if len(liste_tax_taxonomy ) > 1 :
				dictTAX[clade].append(liste_tax_taxonomy[1])
				
# Pour récupérer les valeurs qui nous intéressent, nous allons faire une double lecture à la fois des indices du dictionnaire "dictTAX" et des valeurs de la liste "liste_tax_blast". Si les indices du dictionnaire correspondent aux valeurs de la liste, on compte +1 le résultat. On le stock en tant que valeurs dans un nouveau dictionnaire "dictSIGN_BLAST" qui prend pour clé, les clés du dictionnaire "dictTAX".
	
for i in dictTAX :
	for a in dictTAX[i] :
		for u in liste_tax_blast :
			if u == a : 
				dictSIGN_BLAST[i]+=1
				
# Question n°4 : Calculer le nombre de meilleurs BLAST obtenus entre des fragments et chaque clade. Afficher si ce nombre est > 25.

# On va chercher a comparer le dictionnaire "dictTAX" comportant les noms des clades comme clés et les identifiants de taxons comme valeur avec un dictionnaire d'identifiants de taxons "dictTAXFRAG" liés a des fragments issuent du fichier resultat_Blast1000.tab. Pour récupérer les valeurs qui nous intéressent, nous allons faire une double lecture à la fois des indices du dictionnaire "dictTAX" et à la fois des indices du dictionnaire "dictTAXFRAG". Si les premières clés du dictionnaire "dictTAXFRAG" correspondent aux indices du dictionnaire "dictTAX", on compte +1 le résultat. On le stock en tant que valeurs dans un nouveau dictionnaire "dictBEST_BLAST" qui prend pour clé, les clés du dictionnaire "dictTAX".

for i in dictTAX :
	for a in dictTAX[i] :
		for u in dictTAXFRAG :
			if dictTAXFRAG[u][0] == a : 
				dictBEST_BLAST[i]+=1

# Question n°5 : Combien y-a-t'il de clones pour lesquels tous les gènes significativement similaires aux deux fragments appartiennent au même clade ?
		
for i in dictCLONE2 :
	for a in dictTAX : 
		if all (item in dictTAX[a] for item in dictCLONE2[i]):
			clone_tax +=1

#---------------------Réponse exercice n°3---------------------#

print("\n\n III. Analyse taxonomique des résultats\n")

print("Question n°1 : On retrouve", sumACTINO, "actinobactéries entièrement séquencées dans KEGG.\n")

print("Question n°2 : Les clades dans KEGG ayant plus de 13 génomes séquencés sont : \n ")
for i in sorted(dictCLADE, key=dictCLADE.get, reverse=True) :
	if dictCLADE[i] > 13 :
		print (i,":",dictCLADE[i] )

print("\nQuestion n°3 : Le nombre de BLAST significatifs obtenus entre des fragments et chaque clade est de : \n ")
for i in sorted (dictSIGN_BLAST, key=dictSIGN_BLAST.get, reverse = True) :
	if dictSIGN_BLAST[i] > 100 :
		print (i,":",dictSIGN_BLAST[i])
		graph3x.append(i)
		a=dictSIGN_BLAST[i]
		graph3y.append(a)
print("\nHistogramme - Question III.3.png : ✓")
print("Voir l'image de l'histogramme dans dossier.\n")
		
print("\nQuestion n°4 : Le nombre de meilleurs BLAST obtenus entre des fragments et chaque clade est de : \n ")	
for i in sorted (dictBEST_BLAST, key=dictBEST_BLAST.get, reverse = True) :
	if dictBEST_BLAST[i] > 25 :
		print(i,":",dictBEST_BLAST[i])
	
print("\nQuestion n°5 : Il y a",clone_tax,"clones pour lesquels tous les gènes significativement similaires aux deux fragments appartiennent au même clade.\n")	

#--------------------Variables exercice n°4--------------------#

li_metabo_count = 0
flagxeno=0
li_xeno_count=0
flagaa = 0
li_aa_count=0
ortho_count = 0 
li_count = 0
metabo_count = 0
dict_ortho = defaultdict(int)
dict_voies_metabo = defaultdict(int)
dict_ortho_id_taxon = defaultdict(list)
dicttest = defaultdict(int)
dict_metabo_id_taxon =  defaultdict(list)
dicttest2 = defaultdict(int)
dicttest3 = defaultdict(int)
list_id_tax_sign_actino =[]
dicttest4 = defaultdict(int)
title=defaultdict(list)
graph4y =[]
graph4x = []

#-------------------------Exercice n°4-------------------------#	

# Importation du fichier "pathways" :

with open( "pathways" , "r") as f4 :
	for li in f4 :
		li = li.rstrip()
	
# Question n°1.a : Combien de voies métaboliques sont documentées dans KEGG

# On compte toutes les lignes commençant par "C    ", et on stock le résultat dans une variable, ici   "li_metabo_count".
		
		if li.startswith("A") : #Partie IV - Q4.d
			li_text=re.search("A<B>[\d]{5} (.*)</B>",li) #Partie IV - Q4.d
			title_text=li_text.group(1) #Partie IV - Q4.d
			
		if li.startswith("C") : #Partie IV - Q4.d
			title[title_text]+=re.findall("ko[\d]{5}", li) #Partie IV - Q4.d
			
		if li.startswith ("C    ") :
			li_metabo_count+=1
		
# Question n°1.b : Combien de voies métaboliques ont quelque chose à voir avec les xénobiotiques ?

# On utilise comme précédemment un systéme de flag pour marquer les lignes qui nous intéresses. Comme le terme "xénobiotics" n'est pas écrit de manière uniformisé dans le fichier, on decide de passer les lignes en minuscule. Si la ligne commence par la chaîne de caractère "B  " et quelle comprends la chaîne de caractère "xenobiotics", on la marque à 1. Si la ligne commence par la chaîne de caractère "B  " et quelle ne comprends pas la chaîne de caractère "xenobiotics", on la marque à 0. Pour récupérer le nombre de voies métaboliques ont quelque chose à voir avec les xénobiotiques, il suffit de compter le nombre de lignes marquées à 1 et commençant par la chaîne de caractère "C    ". On stock ensuite le resultat dans une variable nommée ici "li_xeno_count".
		
		if li.startswith ("B  ") and "xenobiotics" in li.lower():
			flagxeno = 1
		if li.startswith ("B  ") and "xenobiotics" not in li.lower():
			flagxeno = 0
		if flagxeno == 1 and li.startswith ("C    ") :
			li_xeno_count+=1

# Question n°1.c : Combien de voies métaboliques sont liées aux acides aminés ?

# On réitére le même schéma en ne changant que le nom des variables et en remplaçant la chaîne de caractère "xenobiotics" par "amino acid". Afin de ne créer aucun conflits, j'ai décidé de changer les valeurs de flag, passant de 0 et 1 à 2 et 3.

		if li.startswith ("B  ") and "amino acid" in li.lower():
			flagaa = 3
		
		if li.startswith ("B  ") and "amino acid" not in li.lower():
			flagaa = 2
		if flagaa == 3 and li.startswith ("C    ") :
			li_aa_count+=1

# Importation du fichier "annotation_genes" :

with open( "annotation_genes" , "r") as f5 :
	for li in f5 :	
		li = li.rstrip()
		li_count += 1
		
# Question n°2.a : Quelle proportion de gènes de KEGG sont affiliés à un groupes d'orthologues ?

		if "; K0" in li or "; K1" in li :
			ortho_count +=1
	
# Question n°2.c : A combien de voies métaboliques un groupe d'orthologue est-il affilié en moyenne ?

			if re.search("; K[\d]{5}", li) :
				ortho = re.findall("K[\d]{5}", li)
		
			for i in ortho :
				dict_ortho[i] = len(re.findall("ko[\d]{5}", li))
			
			moy_voies_metabo = moyenne(dict_ortho.values())
			
# Question n°2.b : Quelle proportion de gènes de KEGG sont affiliés à au moins une voie métabolique ?
		
		if "; ko" in li :
			metabo_count +=1
		
		prop_gene_ortho = ortho_count / li_count * 100			
		prop_gene_metabo = metabo_count / li_count * 100

# Question n°3 : Quel est le nom de la voie la plus présente dans l'annotation des gènes de KEGG ?

		if re.search("ko[\d]{5}", li) :
			voies_metabo = re.findall("ko[\d]{5}", li)
		
			for i in voies_metabo :
				dict_voies_metabo[i] += 1
		
# Question n°4.a : En ne considérant que les résultats significatifs, afficher les 5 groupes d'orthologues les plus représentées dans les résultats de BLAST et leur nombre d'apparition.
		
			ortho = re.findall("K[\d]{5}", li)
		
			li = li.split(" ")
			id_tax_annot_gene = li[0]
		
			dict_metabo_id_taxon[id_tax_annot_gene]=voies_metabo
		
			for i in ortho :
				dict_ortho_id_taxon[i].append(id_tax_annot_gene)

for i in dict_ortho_id_taxon :
	for a in dict_ortho_id_taxon[i] :
		for u in liste_tax_blast_signf :
			if u == a :
				dicttest[i]+=1

# Question n°4.b : Afficher les 10 voies métaboliques les plus représentées et leur fréquence d'apparition (à partir des résultats significatifs).		

for i in dict_metabo_id_taxon:
	for k in dict_metabo_id_taxon[i]:
		for j in liste_tax_blast_signf:
			if i==j:
				dicttest2[k]+=1

# Question n°4.c : Afficher les 10 voies métaboliques les plus représentées parmi les fragments similaires aux actinobactéries et leur fréquence.

for i in liste_tax_blast_signf:
	p=i.split(":")[0]
	for j in id_actino:
		if p==j:
			list_id_tax_sign_actino.append(i)

for i in dict_metabo_id_taxon:
	for k in dict_metabo_id_taxon[i]:
		for j in list_id_tax_sign_actino:
			if i==j:
				dicttest3[k]+=1

# Question n°4.d : Afficher les grandes catégories fonctionnelles les plus représentées et leur fréquence d'apparition (à partir des résultats significatifs) sous la forme d'un graphique.

for i in title:
	for k in title[i]:
		if k in dicttest2:
			dicttest4[i]+=dicttest2[k]

#---------------------Réponse exercice n°4---------------------#

print("\n IV. Analyse du potentiel métabolique de l'écosystème\n")

print("Question n°1 :\n")

print("1.a : On retrouve", li_metabo_count , "voies métaboliques documentées dans KEGG.\n")

print("1.b : Il y a",li_xeno_count,"voies métaboliques en rapport avec les xénobiotiques. \n")

print("1.c : Il y a",li_aa_count,"voies métaboliques liées aux acides aminés.\n")

print("Question n°2 :\n")

print("2.a : La proportion de gènes affiliés à un groupes d'orthologues est de :",round(prop_gene_ortho,3),"%.\n")

print("2.b : La proportion de gènes affiliés affiliés à au moins une voie métabolique &est de :",round(prop_gene_metabo,3),"%.\n")

print("2.c : Un groupe d'orthologue est-il affilié en moyenne a :",round(moy_voies_metabo,3),"voies métaboliques.\n")

for i in sorted (dict_voies_metabo, key=dict_voies_metabo.get, reverse = True) :
	if dict_voies_metabo[i] > 123 :
		print ("Question n°3 : Le nom de la voie la plus présente dans l'annotation des gènes de KEGG est",i,", elle apparait",dict_voies_metabo[i],"fois.\n")
		
print("Question n°4 :\n")		

print("4.a : Les 5 groupes d'orthologues les plus représentées dans les résultats de BLAST sont : \n")	
for i in sorted (dicttest, key=dicttest.get, reverse = True)[:5]:
		print (i, ":", dicttest[i])

print("\n4.b : Les 10 voies métaboliques les plus représentées sont :\n")
for i in sorted (dicttest2, key=dicttest2.get, reverse = True)[:10] :
	print(i,"et sa fréquence est de",round(dicttest2[i]/sum(dicttest2.values())*100, 4),"%")
	
print("\n4.c : Les 10 voies métaboliques les plus représentées parmi les fragments similaires aux actinobactéries sont : \n")
for i in sorted (dicttest3, key=dicttest3.get, reverse = True)[:10] :
	print(i,"et sa fréquence est de",round(dicttest3[i]/sum(dicttest3.values())*100, 4),"%")

print("\n4.d : Les grandes catégories fonctionnelles les plus représentées et leur fréquence d'apparition : \n")
for i in sorted (dicttest4, key=dicttest4.get, reverse = True)[:10] :
	print(i,":",round(dicttest4[i]/sum(dicttest4.values())*100, 4),"%")
	graph4x.append(i)
	a = dicttest4[i]/sum(dicttest4.values())
	graph4y.append(a)
	
print("\nHistogramme - Question IV.4.D.png : ✓ ")
print("Voir l'image de l'histogramme dans dossier.\n")

print("#-------Fin du script-------#")

#---------------------------Graphique--------------------------#

#Graphique - Question III.3

x = graph3x
y = graph3y
plt.figure(figsize=(20,10))
plt.ylabel( "Nombre BLAST significatifs")
plt.xlabel("Clades")
plt.title("Histogramme du nombre de BLAST significatifs obtenus en fonctions de chaque clade")
plt.bar(x, y, align='center', facecolor='#F0492B', alpha=0.75)
plt.savefig("Histogramme - Question III.3.png")

#Graphique - Question IV.4.d

x = graph4x
y = graph4y
plt.figure(figsize=(20,10))
plt.ylabel( "Fréquence")
plt.xlabel("Catégories fonctionnelles")
plt.title("Histogramme des catégories fonctionnelles les plus représentées avec leur fréquence d'apparition")
plt.bar(x, y, align='center', facecolor='#17CBE8', alpha=0.75)
plt.savefig("Histogramme - Question IV.4.D.png")

#--------------------------------------------------------------#
