#FAURIE Lilian


#Aligner de la meilleur facon possible "MVSSD" et "MVSD"
#         match =+1 / unmatch =-1 / gap = -2



#Faire matrice score avec que des zéro

seq1=str(input("Mot1 : "))
seq2=str(input("Mot2 : "))

nbcol=len(seq1)+1
nblig=len(seq2)+1

score = []

for i in range (nblig) :
	nvligne = []
	for j in range (nbcol) :
		nvligne.append(0)
	score.append(nvligne)

#Affichage score

for i in range (nblig) : 
	for j in range (nbcol) :
		print(score[i][j], end="\t")
	print()

print()
print()

#Faire matrice trace avec "deb" dans toutes les cases

nbcol=len(seq1)+1
nblig=len(seq2)+1

trace = []

for i in range (nblig) :
	nvligne = []
	for j in range (nbcol) :
		nvligne.append("deb")
	trace.append(nvligne)

#Affichage trace

for i in range (nblig) : 
	for j in range (nbcol) :
		print(trace[i][j], end="\t")
	print()

print()
print()

#Initialiser la première ligne et colone de la matrice score

match = 1
mismatch = -1
gap = -2


for k in range (1, nbcol) :
	score [0][k] = score [0][k-1]+gap
	#ou score [0][k] = k*gap
	
for t in range (1, nblig) :
	score [t][0] = score [0][t-1]+gap
	#ou score [t][0] = t*gap
		
#Affichage score

for i in range (nblig) : 
	for j in range (nbcol) :
		print(score[i][j], end="\t")
	print()

print()
print()


#Mettre "hor" dans la 1er ligne de trace, "ver" dans la 1er colonne trace

for j in range (1, nbcol) :
	trace [0][j] = "hor"
	
for i in range (1, nblig) :
	trace [i][0] = "ver"

#Affichage trace

for i in range (nblig) : 
	for j in range (nbcol) :
		print(trace[i][j], end="\t")
	print()
		
print()
print()


# Coeur algorithme : remplissage matrice score

for i in range (1, nblig) :
	for j in range (1, nbcol) :
		sver = score [i-1][j] + gap
		shor = score [i][j-1] + gap
		
		if seq1[j-1] == seq2[i-1] :
		
			sdiag = score[i-1][j-1] + match
		else : 
			sdiag = score[i-1][j-1] + mismatch
		
		if sver >= shor and sver >= sdiag :
			score[i][j] = sver
			trace[i][j] = "↑"
		elif shor >= sver and shor >= sdiag :
			score[i][j] = shor
			trace[i][j] = "←"
		else : 
			score [i][j]=sdiag
			trace[i][j] = "↖"

		
#Affichage score

for i in range (nblig) : 
	for j in range (nbcol) :
		print(score[i][j], end="\t")
	print()

print()
print()

#Affichage trace

for i in range (nblig) : 
	for j in range (nbcol) :
		print(trace[i][j], end="\t")
	print()

print()
print()



#Visualisation de l'alignement

lc = nblig -1
cc= nbcol -1
seqa1 = ""
seqa2 = ""

while trace [lc][cc] != "deb":
	if trace[lc][cc] == "↖" :
		seqa1+= seq1[cc-1]
		seqa2+= seq2[lc-1] 
		lc=lc-1
		cc=cc-1
	
	elif trace[lc][cc] == "←" :
		seqa1+= seq1[cc-1]
		seqa2+= "-"
		cc=cc-1
		
	else :
		seqa1+= "-"
		seqa2+= seq2[lc-1] 
		lc=lc-1

print(seqa1[::-1])
print(seqa2[::-1])	


