#!/bin/python3
#FAURIE Lilian 
#M1BI


# Partie I : Créer des graphiques sur les différents virus

# GRAPHIQUE I.1 :
# Bubble plot avec un point par génome
# 	en x : taille des génomes
# 	en y : GC % des génomes
# 	en couleur (gradient de noir à bleu) : la coding capacity (% du génome qui est codant)


#-- Installation pandas [OPTIONNEL] --#

#import sys
#import subprocess

#subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'pandas'])

#------------- Librairies -------------#

import pandas as pd
import re
import os 

#----------- Script Graph 1 -----------#
   

data = pd.read_csv('info_genomes.tsv', delimiter='\t')						# Charger les données depuis un fichier TSV en utilisant pandas


data = data[['Name', 'Length', 'GC', 'nb_prot', 'Locus']]						# Sélectionner les colonnes pertinentes


data_list = []												# Créer une liste de dictionnaires
for _, row in data.iterrows():
    x = row['Length']
    y = row['GC']
    z = round((row['nb_prot']*1000)/row['Length'], 2)
    name = re.sub(r"['\']", ' ', row['Name'])
    locus = row['Locus']
    data_dict = {"x": x, "y": y, "z": z, "Name": name, "Locus": locus}
    data_list.append(data_dict)


with open("datalist_graph_1.txt", "w") as file:							# Écrire les données formatées dans un fichier texte
    for data in data_list:
        data_formatted = "{{x:{}, y:{}, z:{}, Name: '{}', Locus: '{}'}},".format(data['x'], data['y'], data['z'], data['Name'], data['Locus'])
        file.write(str(data_formatted) + '\n')


#----------- HTML,JS,CSS,Python -----------#


html_part1 = ('''
<html>
<head>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/coloraxis.js"></script>


<style>
.highcharts-figure,
.highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
}

#container {
  height: 900px;
  width: 1700px;
  margin: 0 auto;
}

.highcharts-tooltip h3 {
  margin: 0.3em 0;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>

</head>

<body>

<figure class="highcharts-figure">
  <div id="container"></div>
</figure>

<script>


Highcharts.chart('container', {

  chart: {
    height: 900,
    width: 1700,
    type: 'bubble',
    plotBorderWidth: 1,
    zoomType: 'xy'
    
  },

    legend: {
        title: {
            text: 'Coding capacity %<br/>',
            style: {
                fontStyle: 'italic',
                textAlign: 'center'
            }
        },
    },

  title: {
        text: '<b style="font-size: 24px">Taille des génomes en fonction de leurs CG%</b>'
  },
  subtitle: {
        text: 'FAURIE Lilian - M1BI'
  },

  colorAxis: {
        minColor: '#050505',
        maxColor: '#5689db',
  },


  xAxis: {
    type: 'logarithmic',
    minorTickInterval: 500,
    accessibility: {
        rangeDescription: 'Range: 1000 to 150000'
      },
    gridLineWidth: 1,
    title: {
        text: 'log(Genome length (bp))'
    },
    labels: {
        format: '{value}'
    }
  },
  
  yAxis: {
    type: 'logarithmic',
    minorTickInterval: 1,
    accessibility: {
        rangeDescription: 'Range: 1 to 20000'
      },
    gridLineWidth: 1,
    title: {
        text: 'log(CG%)'
    },
    labels: {
        format: '{value}'
    },

  },
  
  tooltip: {
    headerFormat: '<b><table></b><br>',
    pointFormat: '{point.Name}<br>Genome length: {point.x} bp<br>CG%: {point.y}%<br>Coding capacity: {point.z}%'
  },

  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        format: '{point.name}'
      }
    }
  },
  
  plotOptions: {
    bubble: {
      minSize: 1,
      maxSize: 5,
    }
  },

  series: [{
    name: 'Genomes',
    colorKey: 'z',
    data: [
''')

with open('datalist_graph_1.txt', 'r') as f3:
    data_list_content = f3.read()

html_part2 = ('''
    ],  
  }]

});



</script>



</body>
</html>

''')

#----------- Création du fichier HTML -----------#

html_output = html_part1 + data_list_content + html_part2

with open("part1_graph1_.html", "w") as file:
    file.write(html_output)
    
print("\ndatalist_graph_1.txt : OK")    
print("part1_graph1_.html : OK\n")


#GRAPHIQUE I.2 :
#	Bubble plot avec un point par familles virales
#	en x : taille moyenne des génomes
#	en y : nb de génomes
#	une couleur par type de famille ou placer les noms sur le graphique


#----------- Script Graph 2 -----------#


data = pd.read_csv('info_genomes.tsv', delimiter='\t', usecols=['Name', 'family', 'Length'])			# Charger les données depuis un fichier TSV en utilisant pandas


dico = {}														# Création d'un dictionnaire vide
for _, row in data.iterrows():
    name_length_gen = f"{row['Name']} : {row['Length']}"								# Concaténation du nom et de la longueur du génome
    famille = re.sub(r"['\']", ' ', row['family'])									# Récupération de la famille
    if famille not in dico:												# Si la famille n'est pas déjà dans le dictionnaire, on la crée
        dico[famille] = []
    dico[famille].append(name_length_gen)										# Ajout de la ligne courante à la liste correspondante de la famille


for famille, values in dico.items():
    moyenne_taille_genome = sum(int(s.split(' : ')[1]) for s in values) / len(values)				# Calcul de la taille moyenne des génomes pour chaque famille
    dico[famille].append(moyenne_taille_genome)


with open('datalist_graph_2.txt', 'w') as file:									# Affichage du dictionnaire avec la taille moyenne des génomes pour chaque famille
    for famille, values in dico.items():
        nb_genomes = len(values) - 1
        avg_gen_lenght = round(values[-1])
        data_formatted = "{{x:{}, y:{}, z:{}, Name: '{}'}},".format(avg_gen_lenght, nb_genomes, avg_gen_lenght, famille)
        file.write(str(data_formatted) + '\n')


#----------- HTML,JS,CSS,Python -----------#


html_part3 = ('''
<html>
<head>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/coloraxis.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<style>
.highcharts-figure,
.highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
  
}

#container {
  height: 900px;
  width: 1700px;
  margin: 0 auto;
}

.highcharts-tooltip h3 {
  margin: 0.3em 0;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>

</head>

<body>

<figure class="highcharts-figure">
  <div id="container"></div>
</figure>

<script>


Highcharts.chart('container', {

  chart: {
    height: 900,
    width: 1700,
    type: 'bubble',
    plotBorderWidth: 1,
    zoomType: 'xy'
  },

  title: {
        text: '<b style="font-size: 24px">Taille moyenne des génomes en fonction du nombre de génomes par familles</b>'
  },
  subtitle: {
        text: 'FAURIE Lilian - M1BI'
  },
  
  xAxis: {
    type: 'logarithmic',
    accessibility: {
        rangeDescription: 'Range: 1000 to 150000'
      },
    gridLineWidth: 1,
    title: {
        text: 'log(Taille moyenne des génomes en bp)'
    },
    labels: {
        format: '{value}'
    }
  },

  yAxis: {
    type: 'logarithmic',
    minorTickInterval: 1,
    accessibility: {
        rangeDescription: 'Range: 1 to 20000'
      },
    gridLineWidth: 1,
    title: {
        text: 'log(Nombre de genomes)'
    },
    labels: {
        format: '{value}'
    }
  },

  tooltip: {
        useHTML: true,
        headerFormat: '<table>',
        pointFormat: '<tr><th colspan="2"><h3>{point.Name}</h3></th></tr>' +
            '<tr><th>Avg genome lenght:</th><td>{point.x} bp</td></tr>' +
            '<tr><th>Genomes number:</th><td>{point.y}</td></tr>',
        footerFormat: '</table>',
        followPointer: true
    },

  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        format: '{point.name}'
      }
    }
  },
  
  plotOptions: {
    bubble: {
      minSize: 1,
      maxSize: 30,
    }
  },

  series: [{
    name: 'Familles',
    data: [
''')

with open('datalist_graph_2.txt', 'r') as f3:
    data_list_content = f3.read()

html_part4 = ('''
    ],
    colorByPoint: true  
  }]

});



</script>



</body>
</html>

''')

#----------- Création du fichier HTML -----------#

html_output = html_part3 + data_list_content + html_part4

with open("part1_graph2_.html", "w") as file:
    file.write(html_output)
    
print("datalist_graph_2.txt : OK")    
print("part1_graph2_.html : OK\n")


#----------- Création des dossiers et déplacement -----------#

os.system("mkdir HTML_graph")
os.system("mkdir HTML_graph/data_graph")
os.system("mv part1_graph1_.html part1_graph2_.html -t HTML_graph")
os.system("mv datalist_graph_1.txt datalist_graph_2.txt -t HTML_graph/data_graph")

#----------- FIN -----------#
