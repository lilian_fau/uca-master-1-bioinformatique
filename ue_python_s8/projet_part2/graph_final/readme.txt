# FAURIE Lilian 
# M1BI
# README

## Partie I : Créer des graphiques sur les différents virus

Ce répertoire contient un script Python nommé `script_highcharts_part1_graph1_2_FAURIE_Lilian.py` permettant de créer des graphiques représentant des caractéristiques de génomes de virus en utilisant la librairie Highcharts.

### GRAPHIQUE I.1 :

Le script permet de créer un bubble plot dans lequel chaque point représente un génome de virus. Les axes X et Y représentent respectivement la taille des génomes et le % de GC des génomes, tous deux en échelle logarithmique. En couleur, un gradient de noir à bleu représente la coding capacity (% du génome qui est codant).

### GRAPHIQUE I.2 :

Le script permet également de créer un bubble plot représentant la taille moyenne du génome et le nombre de génomes par famille virale.

### Librairies utilisées

* Pandas
* Re

### Installation

Pour exécuter ce script, les librairies pandas et re doivent être installées. Si ce n'est pas déjà fait, elles peuvent être installées avec la commande suivante :

```python
pip install pandas re
```

### Utilisation

Pour utiliser ce script, il est nécessaire de disposer du fichier `info_genomes.tsv`. Ce fichier doit au minimum contenir les colonnes suivantes pour pouvoir par la suite générer les graphiques.

```
Name		Length	GC	nb_prot 	Locus		Familly
AY043533.1	9802	47.75	13		NM_001419	Thermococcaceae
AY053734.1	7607	56.86	14		NM_003714	Pasteurellaceae
AY063935.1	3612	34.98	23		NM_006211	Rhodospirillaceae
...
```

Les colonnes pertinentes pour la création du graphique I.1 sont : "Name", "Length", "GC", "nb_prot", et "Locus"  
Les colonnes pertinentes pour la création du graphique I.2 sont : "Name", "Length" et "Familly"

Une fois que le fichier `info_genomes.tsv` est dans le même répertoire que le script, vous pouvez exécuter le script avec la commande suivante :

```python
python3 script_highcharts_part1_graph1_2_FAURIE_Lilian.py
```

Le script va créer un dossier "HTML_graph" contenant deux fichiers HTML correspondant aux graphiques I.1 et I.2 et ouvrable dans votre navigateur. Le dossier contient également un sous dossier "data_graph" contenant deux fichiers texte nommés "datalist_graph_1.txt" et "datalist_graph_2.txt" correspondant aux données formatées pour les graphiques.  
