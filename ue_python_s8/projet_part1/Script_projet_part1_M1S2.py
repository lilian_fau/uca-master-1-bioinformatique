# FAURIE Lilian

# Etape 3.1 : comparer et clusteriser les protéines avec MMseqs2

#### Modules ####

import os
from os import chdir, getcwd
from collections import defaultdict

#### Variables ####

data_dir = "../Data/Final/"
data_results = "../Data/"
cluster_res = "clusterRes_cluster"
path = getcwd()
list_prot_final = "list_prot_final.txt"
proteins = "proteins.faa"
cat = "cat viral_* | awk -F' : ' '{if ($0 ~/>/){print \">\"$2} else{print $0}}' > proteins.faa"

#### Déplacer et concaténer #####

chdir(data_dir)                 # Déplacement dossier de données d'entrées
os.system(f"{cat}")             # Exécution commande shell fusionnant les fichiers commencant par "phrog_" en un seul fichier "proteins.faa"

print("\nFusion des fichiers : OK\n")

#### Clusterisation du fichier 'proteins.faa' ####

os.system(f"mmseqs easy-cluster proteins.faa {cluster_res} tmp --min-seq-id 0 -c 0.8 --cov-mode 0 > mmseqtmp")          # Clusterisation du fichier "proteins.faa" avec mmseqs2
os.system("rm mmseqtmp")                                                                                                # Suppression des fichiers temporaires
os.system("rm *.fasta")                                                                                                 # Suppression des fichiers .fasta
os.system("rm -r tmp")                                                                                                  # Suppression du dossier "tmp"
os.system("mv proteins.faa ../")                                                                                        # Déplacement du fichier dans la racine antérieur

print("\nClusterisation des protéines : OK\n")
print(f"Fichier {proteins} : OK\n")

#### Création d'un fichier avec la liste des protéines ####

dictID = defaultdict(list)
dictID_sort = defaultdict(list)

with open(cluster_res + "_cluster.tsv", "r") as f2:                                     # Ouverture du fichier "clusterRes_cluster.tsv" en mode lecture
        for li in f2 :                                                                  # Boucle pour chaque ligne dans le fichier
                clé_id_seq = li.split("\t")[0]                                          # Séparation des colonnes en utilisant "\t" comme séparateur et stockage de la colonne >
                indice_id_seq = li.split("\t")[1].rstrip("\n")                          # Séparation des colonnes en utilisant "\t" comme séparateur et stockage de la colonne >
                dictID[clé_id_seq].append(indice_id_seq)                                # Ajout de la valeur indice_id_seq à la clé clé_id_seq dans le dictionnaire dictID
        for i in sorted(dictID, key=lambda i: len(dictID[i]), reverse=True) :           # Boucle pour chaque clé dans dictID, trié en fonction de la longueur de sa liste assoc>
                dictID_sort[i] = dictID[i]                                              # Stockage des données triées dans dictID_sort

with open(list_prot_final, "w") as f3:                                                  # Ouverture du fichier "list_prot.txt" en mode écriture
        for i in dictID_sort:                                                           # Boucle pour chaque clé dans dictID_sort
                li = " ".join(dictID_sort[i])                                           # Conversion de la liste associée à la clé i en une chaîne de caractères
                f3.write(li+"\n")                                                       # Écriture de la chaîne de caractères dans le fichier list_prot.txt

print(f"\nFichier {list_prot_final} : OK\n")                                            # Affichage d'un message indiquant la fin de l'écriture du fichier

os.system("rm " + cluster_res + "_cluster.tsv")                                         # Suppression du fichier "clusterRes_cluster_cluster.tsv"
os.system("mv " + list_prot_final + " " + path + "/" + data_results)                    # Déplacement du fichier "list_prot_final.txt" vers ../Data"

