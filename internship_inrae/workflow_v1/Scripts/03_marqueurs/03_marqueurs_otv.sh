# Récupération des marqueurs OTV qui match avec les gènes

# Activation noeud de calcul HPC2
srun -p gdec -c 1 --pty bash

# Activation bedtools
ml bedtools

# Commande bedtools
bedtools window -a 12.BW_261K_OTV_CS.vcf -b 9.refseqv2.1_annotation_gene_match_F0.01.gff3 -w 1500 > 13.mapping_OTV_match_F0.01.txt

bedtools window -a 12.BW_261K_OTV_CS.vcf -b 9.refseqv2.1_annotation_gene_match_G0.01.gff3 -w 1500 > 13.mapping_OTV_match_G0.01.txt