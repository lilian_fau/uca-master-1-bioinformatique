#Structure code Python

# --- Modification de la structure du fichier fasta --- #
def read_sequences(file_path):
    with open(file_path, 'r') as file:
        sequences = file.read().strip().split('>')[1:]
    return sequences

def read_id_transcript(file_path):
    id_transcript = []
    with open(file_path, 'r') as file:
        lines = file.read().strip().split('\n')[1:]
        id_prot = [line.split('\t')[0] for line in lines]
        transcript = [line.split('\t')[1] for line in lines]
        id_transcript = [f">{prot}/{trans}" for prot, trans in zip(id_prot, transcript)]
    return id_transcript

def write_output_file(id_transcript, sequences, output_file_path):
    with open(output_file_path, 'w') as output_file:
        for i in range(len(id_transcript)):
            output_file.write(f"{id_transcript[i]}-chr{sequences[i]}\n")

sequencesF = read_sequences('Data/2.fasta_list_F0.01.fasta')
sequencesG = read_sequences('Data/2.fasta_list_G0.01.fasta')

id_prot_transcriptF = read_id_transcript('Data/1.uniprot_list_F0.01.tsv')
id_prot_transcriptG = read_id_transcript('Data/1.uniprot_list_G0.01.tsv')

write_output_file(id_prot_transcriptF, sequencesF, 'Data/3.ensemblplants_fasta_F0.01.fasta')
write_output_file(id_prot_transcriptG, sequencesG, 'Data/3.ensemblplants_fasta_G0.01.fasta')
