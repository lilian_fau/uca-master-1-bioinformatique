# Structure code Python

def extract_info(line):
    parts = line.strip().split("/")
    id_protein = parts[0].replace(">", "")
    id_transcript = parts[1].split("-")[0]
    id_chromosome = parts[1].split(":")[3]
    id_start_position = parts[1].split(":")[4]
    id_end_position = parts[1].split(":")[5]
    id_length = int(id_end_position) - int(id_start_position)
    return id_protein, id_transcript, id_chromosome, id_start_position, id_end_position, id_length

def write_table(file_path, id_protein, id_transcript, id_chromosome, id_start_position, id_end_position, id_length):
    with open(file_path, 'w') as output_file:
        output_file.write("ID Protein\tID Gene\tLocalisation\tStart\tStop\tLength\n")
        for i in range(len(id_protein)):
            output_file.write(f"{id_protein[i]}\t{id_transcript[i]}\t{id_chromosome[i]}\t{id_start_position[i]}\t{id_end_position[i]}\t{id_length[i]}\n")

linesF = []
linesG = []

with open('Data/3.ensemblplants_fasta_F0.01.fasta', 'r') as f1:
    linesF = f1.readlines()

with open('Data/3.ensemblplants_fasta_G0.01.fasta', 'r') as f2:
    linesG = f2.readlines()

id_proteinF, id_transcriptF, id_chromosomeF, id_start_positionF, id_end_positionF, id_lengthF = [], [], [], [], [], []
id_proteinG, id_transcriptG, id_chromosomeG, id_start_positionG, id_end_positionG, id_lengthG = [], [], [], [], [], []

for lineF in linesF:
    if lineF.startswith('>'):
        extracted_info = extract_info(lineF)
        id_proteinF.append(extracted_info[0])
        id_transcriptF.append(extracted_info[1])
        id_chromosomeF.append(extracted_info[2])
        id_start_positionF.append(extracted_info[3])
        id_end_positionF.append(extracted_info[4])
        id_lengthF.append(extracted_info[5])

for lineG in linesG:
    if lineG.startswith('>'):
        extracted_info = extract_info(lineG)
        id_proteinG.append(extracted_info[0])
        id_transcriptG.append(extracted_info[1])
        id_chromosomeG.append(extracted_info[2])
        id_start_positionG.append(extracted_info[3])
        id_end_positionG.append(extracted_info[4])
        id_lengthG.append(extracted_info[5])

write_table('Data/4.tableau_recap_F0.01.xlsx', id_proteinF, id_transcriptF, id_chromosomeF, id_start_positionF, id_end_positionF, id_lengthF)
write_table('Data/4.tableau_recap_G0.01.xlsx', id_proteinG, id_transcriptG, id_chromosomeG, id_start_positionG, id_end_positionG, id_lengthG)
