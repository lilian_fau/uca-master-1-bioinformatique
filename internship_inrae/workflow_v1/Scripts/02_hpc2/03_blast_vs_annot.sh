# Structure commande bash

# Activation noeud de calcul HPC2
srun -p gdec -c 1 --pty bash

# Définition des variables avec les noms des fichiers
fichier_refseq="8.refseqv2.1_annotation_gene.gff3"
fichier_blastn_F="7.gene_blastn_cov0.8_F0.01.txt"
fichier_blastn_G="7.gene_blastn_cov0.8_G0.01.txt"

# Extraction des identifiants à partir du fichier de blastn
awk -F'/' '{print $2}' "$fichier_blastn_F" | awk -F'-' '{print $1}' | sort -u | grep -Fwf - "$fichier_refseq" > 9.refseqv2.1_annotation_gene_match_F0.01.gff3
awk -F'/' '{print $2}' "$fichier_blastn_G" | awk -F'-' '{print $1}' | sort -u | grep -Fwf - "$fichier_refseq" > 9.refseqv2.1_annotation_gene_match_G0.01.gff3


# Vérification des ID non match

idtraes_annot=$(awk -F'previous_id=' '{print $2}' 9.refseqv2.1_annotation_gene_match_F0.01.gff3 | awk -F';' '{print $1}' | sort -u)
idtraes_blastn=$(awk -F'/' '{print $2}' "$fichier_blastn_F" | awk -F'-' '{print $1}' | sort -u)
diff <(echo "$idtraes_blastn") <(echo "$idtraes_annot")