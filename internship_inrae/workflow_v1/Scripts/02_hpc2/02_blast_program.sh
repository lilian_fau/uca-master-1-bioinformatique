#Structure code bash: blast.sh

#!/bin/bash
#SBATCH --job-name=Blast 
#SBATCH -o job-%A_%a_task.out
#SBATCH -c 8
#SBATCH --mem=32G
#SBATCH --partition=gdec
#SBATCH --time=1-00:00:00
#SBATCH --export=all
#SBATCH --mail-type=END,FAIL,REQUEUE
#SBATCH --mail-user=lilian.faurie@inrae.fr 

#SetUpScratch
workingdir='/storage/scratch/'$SLURM_JOBID
TMPDIR=$workingdir/tmp
export TMPDIR

### Création et initialisation de l'environnement conda

cd
cd blast/
  ml conda

if ! conda env list | grep -q "blastenv"; then
conda create --name blastenv
fi

conda activate blastenv
ml ncbi-blast

echo -e "\nEnvironnement conda blast : OK"

#SetUpCommands

### Création de la base de donnée

if [[ $(find databases/ -type f -name "*CS2.1_db*") ]]; then
:
  else
    makeblastdb -in input/CS_pseudo_v2.1.fa -dbtype nucl -parse_seqids -out databases/CS2.1_db -title "Chinese Spring v2.1 Database"
fi

echo -e "\nBase de données CS2.1_db : OK"

### Proteins blast (tblastn : proteines --> nucleotides)

echo -e "\ntblastn : En cours (proteines --> nucleotides)"

### Proteins file : work/blast/protein_tblastn_cov0.8_F0.01_result.txt

tblastn -query input/5.uniprot_fasta_F0.01.fasta -db databases/CS2.1_db -out output/6.protein_tblastn_cov0.8_F0.01.txt -num_threads 8 -outfmt 6 -qcov_hsp_perc 80

echo -e "\noutput/6.protein_tblastn_cov0.8_F0.01.txt : OK"

### Proteins file : work/blast/protein_tblastn_cov0.8_G0.01_result.txt

tblastn -query input/5.uniprot_fasta_G0.01.fasta -db databases/CS2.1_db -out output/6.protein_tblastn_cov0.8_G0.01.txt -num_threads 8 -outfmt 6 -qcov_hsp_perc 80

echo -e "\noutput/6.protein_tblastn_cov0.8_G0.01.txt : OK"
echo -e "\ntblastn terminé"

### Genes blast (blastn : nucleotides --> nucleotides)

echo -e "\nblastn: En cours (nucleotides --> nucleotides)"

### Genes file : work/blast/gene_blastn_cov0.8_F0.01_result.txt

blastn -query input/3.ensemblplants_fasta_F0.01.fasta -db databases/CS2.1_db -out output/7.gene_blastn_cov0.8_F0.01.txt -num_threads 8 -outfmt 6 -qcov_hsp_perc 80 -perc_identity 80

echo -e "\noutput/7.gene_blastn_cov0.8_F0.01.txt : OK"

### Genes file : work/blast/gene_blastn_cov0.8_G0.01_result.txt

blastn -query input/3.ensemblplants_fasta_G0.01.fasta -db databases/CS2.1_db -out output/7.gene_blastn_cov0.8_G0.01.txt -num_threads 8 -outfmt 6 -qcov_hsp_perc 80 -perc_identity 80

echo -e "\noutput/7.gene_blastn_cov0.8_G0.01.txt : OK"
echo -e "\nblastn terminé"