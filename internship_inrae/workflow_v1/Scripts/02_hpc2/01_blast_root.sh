# Structure code bash: rootblast.sh

#!/bin/bash
#SBATCH --job-name=Blast 
#SBATCH -o job-%A_%a_task.out
#SBATCH -c 1
#SBATCH --mem=1G
#SBATCH --partition=gdec
#SBATCH --time=1-00:00:00
#SBATCH --export=all

#SetUpModule

### Création des racines dossiers

cd
mkdir blast
mkdir blast/input
mkdir blast/output
mkdir blast/databases

echo Arborescence générée : OK