Vous trouverez dans ce git l'ensemble des datas et scripts utilisés lors de mon stage de bio-informatique Master 1.

Dans chaque version des Workflows, vous trouverez l'arborescence suivante : 

- "Data" contient l'ensemble des données essentielles et intermédiaires nécessaire au bon déroulé des scripts.
- "Images" contient toutes les images contenues dans le Rmarkdown "Notebook-workflow.Rmd".
- "Results" contient uniquement les données essentielles obtenues à l’issue des scripts.
- "Scripts" contient l'ensemble des scripts R/Python/Bash créés et exécuté durant le stage.
- "Notebook - Workflow VX.Rmd" contient toutes les notes du workflow en question.