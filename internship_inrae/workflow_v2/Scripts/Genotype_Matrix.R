library(vcfR)

# Chemin du fichier d'entrée VCF
input_file <- "Data/Shared/BW_261K_corecol_ergecode_withRubisko.vcf"

# Lecture de la 27ème ligne du fichier en sautant les caractères nuls
line_27 <- readLines(input_file, n = 27, skipNul = TRUE)

# Supprimer le premier "1_" de l'identifiant après chaque tabulation (\t) dans la ligne 27
line_27 <- gsub("(?<=\t)1_", "", line_27, perl = TRUE)
#line_27 <- gsub("_", " ", line_27)

# Chemin du fichier tsv à mettre à jour
file_pathF <- "Data/F0.01/File6_mapping_SNP_OTV_F0.01.tsv"
file_pathG <- "Data/G0.01/File6_mapping_SNP_OTV_G0.01.tsv"

# Lecture du contenu actuel du fichier
file_contentF <- readLines(file_pathF)
file_contentG <- readLines(file_pathG)

file_contentF <- lapply(strsplit(file_contentF, "\t"), function(x) paste(x[1:(length(x)-9)], collapse = "\t"))
file_contentG <- lapply(strsplit(file_contentG, "\t"), function(x) paste(x[1:(length(x)-9)], collapse = "\t"))

# Chemin du fichier de sortie VCF
output_fileF <- "Data/F0.01/tmpF.vcf"
output_fileG <- "Data/G0.01/tmpG.vcf"

# Ajout du contenu du fichier tsv au nouveau fichier .vcf
file_contentF <- c(line_27, sapply(file_contentF, paste, collapse = "\t"))
writeLines(file_contentF, output_fileF)

file_contentG <- c(line_27, sapply(file_contentG, paste, collapse = "\t"))
writeLines(file_contentG, output_fileG)

# Charger le package data.table pour la manipulation efficace des données tabulaires
library(data.table)

# Function to split data based on chromosomes
split_data_by_chromosomes <- function(input_file_path, output_folder_path) {
  # Chemin du dossier de sortie pour les fichiers fractionnés
  dir.create(output_folder_path, showWarnings = FALSE, recursive = TRUE, mode = "0755")
  
  # Charger le fichier TSV avec data.table
  data <- fread(input_file_path)
  
  # Obtenir la liste des chromosomes uniques présents dans le fichier
  chromosomes <- unique(data$`#CHROM`)
  
  # Fractionner le fichier en fonction des chromosomes
  for (chromosome in chromosomes) {
    # Extraire les lignes correspondant au chromosome actuel
    subset_data <- data[`#CHROM` == chromosome]
    
    # Générer le nom du fichier de sortie (en utilisant le nom du chromosome)
    output_file_name <- paste0("Marqueurs_", chromosome, ".vcf")
    
    # Chemin complet du fichier de sortie
    output_file_path <- file.path(output_folder_path, output_file_name)
    
    # Écrire le contenu du header suivi des données dans un fichier TSV séparé pour le chromosome actuel
    # Ouvrir le fichier en mode écriture ("w" pour écraser le fichier s'il existe déjà)
    cat(header, file = output_file_path, sep = "\n", append = FALSE)
    
    # Écrire les données dans le fichier
    fwrite(subset_data, file = output_file_path, sep = "\t", col.names = TRUE, quote = FALSE, append = TRUE)
  }
  
  # Afficher un message de confirmation une fois le fractionnement terminé
  cat("Fractionnement des fichiers TSV terminé. Les fichiers fractionnés ont été enregistrés dans le dossier:", output_folder_path)
}

# Function to read the header from the input file
read_header <- function(input_file_path) {
  header <- readLines(input_file_path, n = 26)
  return(header)
}

# Data F processing
input_file_pathF <- "Data/F0.01/tmpF.vcf"
output_folder_pathF <- "Data/F0.01/Matrix/Genotype Matrix F/Input"
header <- read_header(input_file_pathF)
split_data_by_chromosomes(input_file_pathF, output_folder_pathF)
unlink(input_file_pathF) # Supprimer le fichier temporaire "tmpF.vcf"

# Data G processing
input_file_pathG <- "Data/G0.01/tmpG.vcf"
output_folder_pathG <- "Data/G0.01/Matrix/Genotype Matrix G/Input"
header <- read_header(input_file_pathG)
split_data_by_chromosomes(input_file_pathG, output_folder_pathG)
unlink(input_file_pathG) # Supprimer le fichier temporaire "tmpG.vcf"

library(vcfR)
library(dplyr)

process_vcf_files <- function(input_folder, output_folder) {
  # Obtenir la liste de tous les fichiers VCF dans le dossier d'entrée
  vcf_files <- list.files(input_folder, pattern = "\\.vcf$", full.names = TRUE)
  
  # Parcourir chaque fichier VCF dans la liste
  for (vcf_file in vcf_files) {
    # Lire le fichier VCF et extraire les génotypes
    vcf <- read.vcfR(vcf_file, verbose = FALSE)
    genotypes <- extract.gt(vcf, as.numeric = TRUE)
    
    # Obtenir les identifiants et les coordonnées de chaque marqueur (SNP) dans le fichier VCF
    chr <- vcf@fix[, "CHROM"]
    position <- vcf@fix[, "POS"]
    markers <- vcf@fix[, "ID"]
    
    # Ajouter les colonnes "chr" et "position" au début du dataframe 'genotypes'
    genotypes <- cbind(marker = markers, chrom = chr, pos = position, genotypes)
    
    # Créer le nom du fichier de sortie en remplaçant l'extension .vcf par .tsv
    output_tsv_file <- gsub(".vcf$", ".tsv", basename(vcf_file))
    output_tsv_file <- paste("Matrice_", output_tsv_file, sep = "")
    output_tsv_file <- file.path(output_folder, output_tsv_file)
    
    # Sauvegarder le nouveau dataframe dans un fichier TSV
    write.table(genotypes, file = output_tsv_file, sep = "\t", quote = FALSE, row.names = FALSE, col.names = TRUE)
  }
  cat("Les fichiers .vcf ont été traités et les matrices ont été écrites dans:", output_folder, "\n")
}


# Invert MATRIX
# Appel de la fonction pour le premier cas
input_folder_1 <- "Data/F0.01/Matrix/Genotype Matrix F/Input/"
output_folder_1 <- "Data/F0.01/Matrix/Genotype Matrix F/Output/"
process_vcf_files(input_folder_1, output_folder_1)

# Appel de la fonction pour le deuxième cas
input_folder_2 <- "Data/G0.01/Matrix/Genotype Matrix G/Input/"
output_folder_2 <- "Data/G0.01/Matrix/Genotype Matrix G/Output/"
process_vcf_files(input_folder_2, output_folder_2)


# MATRIX
# Appel de la fonction pour le premier cas
input_folder_3 <- "Data/F0.01/Matrix/Genotype F/Input/"
output_folder_3 <- "Data/F0.01/Matrix/Genotype F/Output/"
process_vcf_files(input_folder_3, output_folder_3)

# Appel de la fonction pour le deuxième cas
input_folder_4 <- "Data/G0.01/Matrix/Genotype G/Input/"
output_folder_4 <- "Data/G0.01/Matrix/Genotype G/Output/"
process_vcf_files(input_folder_4, output_folder_4)