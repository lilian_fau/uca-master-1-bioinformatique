#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""align.py
Alignement de Needleman et Wunsch et comparaison multiple de séquences nucléotidique selon la méthode de l'étoile centrale.
===== Ceci est une description. Libre à vous d'écrire ce que bon vous chante.

AUTHOR
Bidule Machin

VERSION
1.0

DATE
03/03/20
"""

###### Importation des modules ######

from collections import defaultdict						# Module dictionnaires
import argparse 								# Pour passer des arguments à un script
import logging as log 								# Pour avoir un mode verbeux
import sys 									# Module interpreteur système

sys.path.append("modules/")							# Chemin d'accès au dossier "modules"
import fasta_align as ali							# Importation du module "fasta_align" en "ali"
import fasta_parser as par							# Importation du module "fasta_parser" en "par"


###### Coder la méthode de l'étoile centrale ######

# Pour chaque sequence :
# Comparer 2 à 2 avec toutes les séquences autres qu'elle même à l'aide de la fonction needleman (avec match = 10, mismatch = -10 et gap = -10).
# additionner le score de chacune de ces comparaisons pour obtenir un score global de similitude avec l'ensemble des séquences pour chaque séquence

def main(verbose, seq, match, mismatch, gap):

	dictID_seq = par.parse_multiple(seq)

	for i in dictID_seq:
		score_total = 0
		for a in dictID_seq:
			if a !=i:
				score,al1,al2 = ali.needleman(dictID_seq[i],dictID_seq[a],match, mismatch, gap)
				score_total += score
		print("Le nom de la séquence est :", i) 
		print("Le score de la séquence est :",score_total) 
		print("La séquence nucléotidique associée est :", dictID_seq[i],"\n")

# printer la séquence avec le score le plus élevé, cad la séquence la plus proche de toutes les autres séquences.

def _cli():
    parser = argparse.ArgumentParser(
            description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            argument_default=argparse.SUPPRESS)
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "Boolean: activate verbose mode. Default is no verbose.")
    parser.add_argument('-s', '--seq', help = "Path to  multiple_sequences.fasta file")
    parser.add_argument('-m', '--match', default = 10, help = "match score")
    parser.add_argument('-msm', '--mismatch', default = -10, help = "mismatch score")
    parser.add_argument('-gap', '--gap', default = -10, help="gap score")
            
    args = parser.parse_args()
    
    if args.verbose == True :
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        log.info("Verbose output.")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s")

    return vars(args)

if __name__ == '__main__':
    main(**_cli())


