def needleman(sequence1, sequence2, match, mismatch, gap): # fonction d'alignement de deux séquences nucléotidiques avec l'algorithme de Needleman et Wunsch
    
### création et initialisation des matrices de score et de trace ###

    nbcol = len(sequence1) + 1 #nombre de caractere + 1 : colones
    nblig = len(sequence2) + 1 #nombre de caractere + 1 : lignes
    score = [[0 for x in range(nbcol)] for y in range(nblig)] #creation de la matrice de score
    trace = [['x']*nbcol for x in range (nblig)]#Creation de la matrice de trace

    for c in range (nbcol): # pour chaque colonne :
        score [0][c]= c*gap 
        trace [0][c]= 'h' 
    for c in range(nblig): # pour chaque ligne :
        trace [c][0]= 'v'
        score [c][0] = c*gap  
    trace [0][0] = 'D'
    
### remplissage des matrices de score et de trace ###

    for lc in range(nblig-1): 
        for cc in range (nbcol-1):
            if sequence1[cc] == sequence2[lc]:
                d = score[lc][cc] + match
            else :
                d = score[lc][cc] + mismatch
            
            h = score [lc+1][cc] + gap
            v = score [lc][cc+1] + gap
            
            if d >= h and d >= v: 
                score[lc+1][cc+1] = d
                trace[lc+1][cc+1] = 'd'
            elif h >= v:
                score[lc+1][cc+1] = h
                trace[lc+1][cc+1] = 'h'
            else :
                score[lc+1][cc+1] = v
                trace[lc+1][cc+1] = 'v'
                
### création de l'alignement à partir du meilleur chemin ###
    
    alig1 = ""
    alig2 = ""
    
    a = len(sequence1)
    b = len(sequence2)
    
    while a >=0 and b >=0 : 

        if trace [b][a]== 'd' :
            a = a -1
            b = b - 1
            alig1 =  sequence1[a] +alig1 
            alig2 =  sequence2[b] +alig2 
        elif trace [b][a] == 'h':
            a = a -1
            alig1 =  sequence1[a] + alig1
            alig2 = "-" + alig2 
        elif trace [b][a] == 'D':
            a = a -1
            b = b - 1
        else:
            b = b -1
            alig2 = sequence2[b] + alig2 
            alig1 = "-" + alig1
            
### renvoie du meilleur score et de l'alignement ###
        
    topscore = score[nblig-1][nbcol-1]
    return topscore, alig1, alig2
    
