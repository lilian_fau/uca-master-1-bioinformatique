from collections import defaultdict

def parse_simple(fichier):								# Définition de la fonction "parse_simple"		
	nom = ""
	sequence = ""
	with open(fichier, "r") as f1:						# Ouverture du fichier en lecture
		for li in f1:								# Parcours du fichier ligne à ligne
			if li.startswith(">"):						# Si la ligne commence par ">", alors elle correspond au nom de la séquence
				nom = li[1:].strip()					# On récupere le nom de la séquence sans le chevron
			else:								# Sinon
				sequence += li.strip()					# On récupère la ligne correspondante a la séquence et on l'ajoute à la variable "sequence"
		return nom, sequence							# La fonction renvoie le nom de la séquence et sa séquence nucléotidique correspondante

"""
nom, sequence = parse_simple("pip11.fasta")
print()
print("Le nom de la séquence est :", nom)
print("La séquence nucléotidique associée est :", sequence)
"""

###### définir la fonction parse_multiple qui, à partir d'un fichier fasta multiple, associe dans un dictionnaire chaque séquence à un nom et qui renvoie ce dictionnaire. ######

dictID_seq = defaultdict(str)								# Définition du dictionnaire "dictID_seq"

def parse_multiple(fichier):								# Définition de la fonction "parse_multi"	
	with open(fichier, "r") as f2:						# Ouverture du fichier en lecture
		nom = ""
		for li in f2:								# Parcours du fichier ligne à ligne
			li = li.strip("\n")						# On supprime les caractères blancs dans la ligne
			if li.startswith(">"):						# Si la ligne commence par un ">"
				nom = li[1:]						# On extrait le nom  
				dictID_seq[nom] = ""					# Et on créer avec une clé dans le dictionnaire dictID_seq 
			else:								# Sinon
				sequence = li						# On stock la ligne dans une variable "sequence"
				dictID_seq[nom] += sequence				# On ajoute la séquence en tant qu'indice à la clé correspondante dans le dictionnaire
		return dictID_seq							# On retourne le dictionnaire qui associe chaque nom à sa séquence correspondante
