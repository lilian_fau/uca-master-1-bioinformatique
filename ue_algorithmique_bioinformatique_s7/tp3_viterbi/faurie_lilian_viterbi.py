#FAURIE Lilian
#!/bin/python3


#Librairies nécessaires
from collections import defaultdict
from math import log
from numpy import zeros,array
import re

#HMM

#Algorithme de Viterbi : decoding (& scoring) problem

#Objectif : inférer la suite d'états la plus probable à partir d'une séquence d'observations

#####################################
# 1 ) Définition du HMM 
#####################################

#Liste d'observations possibles (nucléotides)

obs=["a","c","g","t"]

#Liste d'états possibles

state_list=[]
state_list=["0","A","C","G","T","a","c","g","t"]

#Afficher la liste d'états

print(f"\nLa liste d'états est : {state_list} \n")

#L'état 0 est la source 

etat_zero = state_list[0]

#Probabilités d'émission

#Dictionnaire à deux dimensions
#Clé 1 : état
#Clé 2 : observation
#Valeur : probabilité

#Ouvrir le fichier Matrice_emis.tsv et remplir le dictionnaire emit_p
#Transformer toutes les probabilités au log()
#Si la probabilité est nulle, elle devient log(1e-100) car log(0) n'existe pas 

emit_p=defaultdict(dict)

l=0
with open("Matrice_emis.csv","r") as f1 :
	for li in f1 :
		lli = li.strip().split()
		for j in range(len(lli)) :
			i = float(lli[j])
			if i == 0 :
				i = round(log(1e-100),3)
			else :
				i = round(log(i),3)
			emit_p[state_list[l]][obs[j]] = i
		l+=1

print (f"Dictionnaire des probabilités d'émission :\n\n{emit_p}\n")	

#Probabilités de transition

#Dictionnaire à deux dimensions
#Clé 1 : état 1
#Clé 2 : état 2
#Valeur : probabilité


#Ouvrir le fichier Matrice_trans.tsv et remplir le dictionnaire transi_p
#Transformer toutes les probabilités au log()
#Si la probabilité est nulle, elle devient log(1e-100) car log(0) n'existe pas 

transi_p=defaultdict(dict)

w=0
with open("Matrice_trans.csv","r") as f2 :
	for li in f2 :
		lli = li.strip().split()
		for j in range(len(lli)) :
			i = float(lli[j])
			if i == 0 :
				i = round(log(1e-100),3)
			else :
				i = round(log(i),3)
			transi_p[state_list[w]][state_list[j]] = i
		w+=1
				
print (f"Dictionnaire des probabilités de transition :\n\n{transi_p}\n")

#####################################
# 2 ) Initialisation 
#####################################

#Probabilités de départ = transition de 0 à K
#Afficher ces probabilités

#Variable de Viterbi

#Dictionnaire à deux dimensions
#Clé 1 : position sur la séquence test (i)
#Clé 2 : état (state_list) (E)
#Valeur : probabilité 

viterbi=defaultdict(dict)

#Intitalisation de la variable de Viterbi :
#À la position "source" seul l'état 0 peut être émis

#Remplir la matrice de Viterbi pour chaque état à la position 0
#Pour l'état 0, viterbi = 0 (valeur maximale)
#Pour tous les autres états, viterbi = log(1e-100) (valeur minimale)

for state in state_list:
	viterbi[0]['0'] = 0
	if state == 0:
		viterbi[0][state] = 0
	else:
		viterbi[0][state] = log(1e-100)

print("\nMatrice de Viterbi pour la position 0 :\n")

for state in state_list:
	print("Etat {} : {}".format(state, round(viterbi[0][state], 2)))

#####################################
# 3 ) Scoring 
#####################################

#Séquence de test = séquence d'observations

#Lire le fichier correspondant et mettre la séquence nucléotidique dans la variable test_sequence
#Sans prendre en compte le nom de la séquence
#Ouvrir le fichier testing_CpG.fa et stocker la séquence

with open("testing_CpG.fa","r") as test_sequence_file:
	test_sequence = "".join(test_sequence_file.readlines()[1:])
	test_sequence = test_sequence.replace("\n","")

#Dictionnaire à deux dimensions pour gader en mémoire l'état précédent
#Clé 1 : position sur la séquence test
#Clé 2 : état courant
#Valeur : état précédent

etat_prec = defaultdict(dict)

#Rajouter de l'état "source" dans la séquence pour que l'initialisation soit correcte

test_sequence="0"+test_sequence

#Scoring
#Calcul du score de chaque état à chaque position
#Remplissage de la matrice de viterbi

#À chaque position de la séquence remplir la matrice de viterbi (le dictionnaire viterbi)
#Parcourir les états courants et précédents pour trouver le meilleur score à chaque case de la matrice
#Ne pas oublier d'initialiser la valeur de viterbi à une valeur minimale, car dans un defaultdict la valeur par défaut est 0
#Calculer la nouvelle valeur de viterbi : 
#newscore=viterbi[i-1][P] + emit_p[E][test_sequence[i]] + transi_p[P][E]
#Stocker la valeur maximale de viterbi dans le dictionnaire pour chaque état
#viterbi[i][E]=newscore
#Stocker l'état précédent qui a donné cette valeur

for i in range(1,len(test_sequence)) :
	x = test_sequence[i].lower()
	for K in state_list :
		viterbi[i][K] = float('-inf')
		emission = emit_p[K][x]
		for L in state_list :
			transition = transi_p[L][K]
			new_score = viterbi[i-1][L]+transition+emission
			if new_score > viterbi[i][K] :
				viterbi[i][K] = new_score
				etat_prec[i][K] = L

#en postion x l'etat 0 a etait obtenu par l'etat t 
	
#####################################
# 4 ) Decoding 
#####################################

#À partir de la matrice des scores on veut retrouver le chemin qui mène au meilleur score de Viterbi

# a) Aller chercher l'état final, avec le score maximum (valeur maximale de la variable de Viterbi)
#À la position finale de la séquence
#Stocker le score de la séquence la plus probable dans (maxi)
#Stocker l'état final dans (max_state)

derniere_entree = list(viterbi.items())[-1]
maxi = max(derniere_entree[1].values())
max_state = max(derniere_entree[1], key=derniere_entree[1].get)

#Stocker la séquence d'états la plus probable dans :

best_chemin=""
best_chemin2=""

#Avec une boucle for, recupérer le meilleur score 'maxi' dans la dernière colonne de la matrice de Viterbi
#Ce score est associé au dernier état inféreé => récupérer cet état maximal maximal et le stocker dans 'max_state'

#Rajouter l'état final au meilleur chemin
best_chemin+=max_state[-1]

#b) decoding : retrouver la séquence d'états la plus probable 
#En utilisant l'état précedent de l'état maximal
#Cet état précédent deviens maintenant l'état maximal, il faut récupérer son état précédent
#Et ainsi de suite
#Écrire ces états précédents dans best_chemin, ATTENTION vous parcourez la matrice de Viterbi à l'envers

for i in reversed(range(3,len(test_sequence))):
	best_chemin2+=etat_prec[i][max_state]
	max_state=etat_prec[i][max_state]
for i in reversed(range(0,len(best_chemin2))):
	best_chemin+=best_chemin2[i]

#####################################
# 5 ) Score final et position des îlots CpG
#####################################

#Afficher le meilleur score 'maxi'

print(f"\nLa valeur de score maximal est de : {maxi}")

#Retrouver les îlots CpG (les états en majuscule, ou avec un +) dans 'best_chemin' 
#Vous pouvez utiliser une expression régulière
statut = 0
a = 0
pattern = re.compile("[ATCG]+")
print("\nLes îlots CpG sont les suivants :\n")
print("	Début	Fin	Longueur")
while pattern.search(best_chemin, statut) !=None :
	deb,fin=pattern.search(best_chemin,statut).span()
	length=fin - deb + 1
	statut = fin
	a+=1
	
#Afficher la position et la taille de ces îlots

	print(f"Îlot {a}  {deb} \t {fin} \t {length}")

#Afficher la séquence d'états 'best_chemin' pour avoir un aperçu de la séquence

print(f"\nLa séquence d'états : {best_chemin}")

#FIN







